//
//  PhotosViewController.h
//  ScanDroboxPhotos
//
//  Created by Alexander on 12/09/15.
//  Copyright (c) 2015 Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropboxSDK/DropboxSDK.h"

@interface PhotosViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIAlertViewDelegate>
{
    DBRestClient * restClient;
    BOOL isUploading;
}

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *btnRandom;
@property (weak, nonatomic) IBOutlet UIButton *btnAddPhoto;

@property (assign, nonatomic) BOOL working;
@property (strong, nonatomic) NSArray * photoPaths;
@property (strong, nonatomic) NSString * photosHash;
@property (strong, nonatomic) NSString * currentPhotoPath;

@property (strong, nonatomic) UIImagePickerController * pickerView;
@property (strong, nonatomic) UIImage * pickedImage;

@property (strong, nonatomic) NSTimer * timer;

- (IBAction)btnRandomPressed:(id)sender;
- (IBAction)btnAddPhotoPressed:(id)sender;
@end

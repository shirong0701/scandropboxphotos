//
//  GalleryViewController.m
//  ScanDroboxPhotos
//
//  Created by Alexander on 13/09/15.
//  Copyright (c) 2015 Alexander. All rights reserved.
//

#import "GalleryViewController.h"

@interface GalleryViewController ()

@property (readonly, nonatomic) DBRestClient * restClient;

@end

@implementation GalleryViewController

@synthesize restClient;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (restClient == nil) {
        restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        restClient.delegate = self;
    }
    
    isUploading = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    _thumbArray = [[NSMutableArray alloc] init];
    _imgeArray = [[NSMutableArray alloc] init];
    library = [[ALAssetsLibrary alloc] init];
    assetGroups = [[NSMutableArray alloc] init];
    
    // Load Albums into assetGroups
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       // Group enumerator Block
                       void (^assetGroupEnumerator)(ALAssetsGroup *, BOOL *) = ^(ALAssetsGroup *group, BOOL *stop)
                       {
                           if (group == nil)
                           {
                               return;
                           }
                           
                           [assetGroups addObject:group];
                           //NSLog(@"assetGroups : %@",assetGroups);
                           assetGroup = [assetGroups objectAtIndex:0];
                           if ([assetGroups count] > 0)
                           {
                               [self performSelectorInBackground:@selector(preparePhotos) withObject:nil];
                           }
                           
                       };
                       
                       // Group Enumerator Failure Block
                       void (^assetGroupEnumberatorFailure)(NSError *) = ^(NSError *error) {
                           
                           UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"Album Error: %@ - %@", [error localizedDescription], [error localizedRecoverySuggestion]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                           [alert show];
                           
                           NSLog(@"A problem occured %@", [error description]);
                       };
                       
                       // Enumerate Albums
                       [library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos
                                              usingBlock:assetGroupEnumerator
                                            failureBlock:assetGroupEnumberatorFailure];
                       
                   });

}

-(void)preparePhotos
{
    [assetGroup enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop)
     {
         if(result == nil)
         {
             return;
         }
         
         //NSDate* date = [result valueForProperty:ALAssetPropertyDate];
         UIImage * imgThumb = [UIImage imageWithCGImage:[result thumbnail]];
         [_thumbArray addObject:imgThumb];
         
         ALAssetRepresentation * assetRepresentation = [result defaultRepresentation];
         UIImage * imgOriginal = [UIImage imageWithCGImage:[assetRepresentation fullScreenImage] scale:[assetRepresentation scale] orientation:UIImageOrientationUp];
         [_imgeArray addObject:imgOriginal];
     }];
    
    [_galleryView reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnCancelPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UICollectionView Delegate Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_thumbArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    NSInteger cellWidth = _galleryView.frame.size.width / 3 - 10;
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cellWidth, cellWidth)];
    [imageView setImage:[_thumbArray objectAtIndex:indexPath.row]];
    [cell.contentView addSubview:imageView];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger cellWidth = collectionView.frame.size.width / 3 - 10;
    return CGSizeMake(cellWidth, cellWidth);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (!isUploading) {
        _selectedIndex = indexPath.row;
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"ScanDropbox" message:@"Are you sure?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Sure", nil];
        
        [alert show];
    }
}

#pragma mark - DBRestClient Delegate Methods

- (void)restClient:(DBRestClient *)client uploadedFile:(NSString *)destPath from:(NSString *)srcPath metadata:(DBMetadata *)metadata
{
    NSLog(@"File uploaded successfully to path : %@", metadata.path);
    [_activityIndicator stopAnimating];
}

- (void)restClient:(DBRestClient *)client uploadFileFailedWithError:(NSError *)error
{
    NSLog(@"File uploade failed with error: %@", error);
    isUploading = NO;
    [_activityIndicator stopAnimating];
}

#pragma mark - UIAlertViewDelegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        isUploading = YES;
        [_activityIndicator startAnimating];
        
        UIImage * img = [_imgeArray objectAtIndex:_selectedIndex];
        NSData * imgData = UIImageJPEGRepresentation(img, 1.0f);
        
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString * imgPath = [paths objectAtIndex:0];
        imgPath = [imgPath stringByAppendingPathComponent:@"/image.jpg"];
        
        NSError * error = nil;
        BOOL success = [imgData writeToFile:imgPath options:NSDataWritingAtomic error:&error];
        
        NSString * filename = [NSString stringWithFormat:@"%@.jpg",[[NSUUID UUID] UUIDString]];
        NSString * desDir = [NSString stringWithFormat:@"%@", filename];
        if (success) {
            [restClient uploadFile:filename toPath:desDir withParentRev:nil fromPath:imgPath];
        }
        else
        {
            NSLog(@"error : %@", error);
            [_activityIndicator stopAnimating];
        }
    }
}

@end

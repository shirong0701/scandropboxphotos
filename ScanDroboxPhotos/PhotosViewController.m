//
//  PhotosViewController.m
//  ScanDroboxPhotos
//
//  Created by Alexander on 12/09/15.
//  Copyright (c) 2015 Alexander. All rights reserved.
//

#import "PhotosViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface PhotosViewController ()<DBRestClientDelegate, UIActionSheetDelegate>

@property (readonly, nonatomic) DBRestClient * restClient;

@end

@implementation PhotosViewController

@synthesize restClient;

- (void)viewDidLoad {

    [super viewDidLoad];
    isUploading = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self autoLoadPhotos];
}

- (void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];

}

- (void)loadRandomPhoto
{
    if ([_photoPaths count] == 0 ) {
        NSString * msg = nil;
        
        if ([DBSession sharedSession].root == kDBRootDropbox) {
            msg = @"Put .jpg photos in your Photos folder to use DBRoulette!";
        }
        else
            msg = @"Put .jpg photos in your app's App folder to use DBRoulette!";
        
        [[[UIAlertView alloc]
          initWithTitle:@"No Photos!" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]
         show];
        
        [self setWorking:NO];
    }
    else
    {
        NSString * photoPath;
        if ([_photoPaths count] == 1) {
            photoPath = [_photoPaths objectAtIndex:0];
            if ([photoPath isEqual:_currentPhotoPath]) {
                [[[UIAlertView alloc]
                  initWithTitle:@"No More Photos" message:@"You only have one photo to display."
                  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]
                 show];
                
                [self setWorking:NO];
                return;
            }
        } else {
            // Find a random photo that is not the current photo
            do {
                srandom((unsigned int)time(NULL));
                NSInteger index =  random() % [_photoPaths count];
                photoPath = [_photoPaths objectAtIndex:index];
            } while ([photoPath isEqual:_currentPhotoPath]);
        }
        
        _currentPhotoPath = photoPath;
        
        [restClient loadThumbnail:_currentPhotoPath ofSize:@"iphone_bestfit" intoPath:[self photoPath]];
    }
}

- (NSString *)photoPath
{
    return [NSTemporaryDirectory() stringByAppendingPathComponent:@"photo.jpg"];
}

- (void)displayError {
    [[[UIAlertView alloc]
      initWithTitle:@"Error Loading Photo" message:@"There was an error loading your photo."
      delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]
     show];
}


- (void)setWorking:(BOOL)isWorking
{
    if (_working == isWorking) return;
    _working = isWorking;
    
    if (_working) {
        [_activityIndicator startAnimating];
    }
    else
        [_activityIndicator stopAnimating];
    
    _btnRandom.enabled = !_working;
}

- (DBRestClient*)restClient {
    if (restClient == nil) {
        restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        restClient.delegate = self;
    }
    return restClient;
}

#pragma mark - DBRestClientDelegate methods

- (void)restClient:(DBRestClient *)client loadedMetadata:(DBMetadata *)metadata
{
    _photosHash = metadata.hash;
    NSArray * validExtentions = [NSArray arrayWithObjects:@"jpg", @"jpeg", @"png", nil];
    NSMutableArray * newPhotoPaths = [[NSMutableArray alloc] init];
    for (DBMetadata * child in metadata.contents) {
        NSString * extention = [[child.path pathExtension] lowercaseString];
        if (!child.isDirectory && [validExtentions indexOfObject:extention] != NSNotFound) {
            [newPhotoPaths addObject:child.path];
        }
    }
    
    _photoPaths = newPhotoPaths;
    [self loadRandomPhoto];
}

- (void)restClient:(DBRestClient *)client metadataUnchangedAtPath:(NSString *)path
{
    [self loadRandomPhoto];
}

- (void)restClient:(DBRestClient *)client loadMetadataFailedWithError:(NSError *)error
{
    [self displayError];
    [self setWorking:NO];
}

- (void)restClient:(DBRestClient *)client loadedThumbnail:(NSString *)destPath
{
    [self setWorking:NO];
    _imageView.image = [UIImage imageWithContentsOfFile:destPath];
    
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
    
    if (!isUploading) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:3.0f target:self selector:@selector(autoLoadPhotos) userInfo:nil repeats:NO];
    }
}

- (void)restClient:(DBRestClient *)client loadThumbnailFailedWithError:(NSError *)error
{
    [self setWorking:NO];
    [self displayError];
}

- (void)restClient:(DBRestClient *)client uploadedFile:(NSString *)destPath from:(NSString *)srcPath metadata:(DBMetadata *)metadata
{
    NSLog(@"File uploaded successfully to path : %@", metadata.path);
    isUploading = NO;
    [self setWorking:NO];
    [self autoLoadPhotos];
}

- (void)restClient:(DBRestClient *)client uploadFileFailedWithError:(NSError *)error
{
    NSLog(@"File uploade failed with error: %@", error);
    [self setWorking:NO];
}

- (void)autoLoadPhotos
{
    [self setWorking:YES];
    
    NSString * photosRoot = nil;
    if ([DBSession sharedSession].root == kDBRootDropbox) {
        photosRoot = @"Photos";
    }
    else
        photosRoot = @"/";
    [self.restClient loadMetadata:photosRoot withHash:_photosHash];
}

- (IBAction)btnRandomPressed:(id)sender {
    [self setWorking:YES];
    
    NSString * photosRoot = nil;
    if ([DBSession sharedSession].root == kDBRootDropbox) {
        photosRoot = @"Photos";
    }
    else
        photosRoot = @"/";
    [self.restClient loadMetadata:photosRoot withHash:_photosHash];
}

- (IBAction)btnAddPhotoPressed:(id)sender {
    UIActionSheet * sheet = [[UIActionSheet alloc] initWithTitle:@"Select One"
                                               delegate:self
                                               cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                               otherButtonTitles:nil];
    [sheet addButtonWithTitle:@"From Camera"];
    [sheet addButtonWithTitle:@"From Gallery"];
    
    sheet.cancelButtonIndex = [sheet addButtonWithTitle:@"Cancel"];
    
    [sheet showInView:self.view];

    isUploading = YES;
    [self setWorking:NO];
    
    [_timer invalidate];
    _timer = nil;
}

#pragma mark - UIActionSheetDelegate Methods

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        NSLog(@"From Camera");
        
        _pickerView = [[UIImagePickerController alloc] init];
        [_pickerView setDelegate:self];
        [_pickerView setSourceType:UIImagePickerControllerSourceTypeCamera];
        [_pickerView setAllowsEditing:YES];
        
        [self presentViewController:_pickerView animated:YES completion:nil];
    }
    else if (buttonIndex == 1)
    {
        [self performSegueWithIdentifier:@"showGalleryViewController" sender:nil];
    }
    else
    {
        [self autoLoadPhotos];
    }
}

#pragma mark - UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];

    if([mediaType isEqualToString:(__bridge NSString *)kUTTypeImage]) {
        _pickedImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"ScanDropbox" message:@"Are you sure?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Sure", nil];
        
        [alert show];
    }
}

#pragma mark - UIAlertViewDelegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        isUploading = YES;
        [self setWorking:YES];
        
        NSData * imgData = UIImageJPEGRepresentation(_pickedImage, 1.0f);
        
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString * imgPath = [paths objectAtIndex:0];
        imgPath = [imgPath stringByAppendingPathComponent:@"/image.jpg"];
        
        NSError * error = nil;
        BOOL success = [imgData writeToFile:imgPath options:NSDataWritingAtomic error:&error];
        
        NSString * filename = [NSString stringWithFormat:@"%@.jpg",[[NSUUID UUID] UUIDString]];
        NSString * desDir = [NSString stringWithFormat:@"%@", filename];
        if (success) {
            [self.restClient uploadFile:filename toPath:desDir withParentRev:nil fromPath:imgPath];
        }
        else
        {
            NSLog(@"error : %@", error);
            [self setWorking:NO];
        }
    }
    else
    {
        [self autoLoadPhotos];
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
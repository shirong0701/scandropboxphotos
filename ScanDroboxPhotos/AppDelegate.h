//
//  AppDelegate.h
//  ScanDroboxPhotos
//
//  Created by Alexander on 12/09/15.
//  Copyright (c) 2015 Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropboxSDK/DropboxSDK.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString * reLinkUserId;

@end


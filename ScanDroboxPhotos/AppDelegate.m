//
//  AppDelegate.m
//  ScanDroboxPhotos
//
//  Created by Alexander on 12/09/15.
//  Copyright (c) 2015 Alexander. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()<DBSessionDelegate, DBNetworkRequestDelegate, UIAlertViewDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    NSString * dropboxAppKey = @"7y1rvv7500o1b1m";
    NSString * dropboxAppSecret = @"y6bepc65o0n1ldx";
    NSString * dropboxAppType = kDBRootAppFolder;
    
    NSString * errMsg = nil;
    if ([dropboxAppKey rangeOfCharacterFromSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]].location != NSNotFound) {
        errMsg = @"Make sure you set the app key correctly in DBRouletteAppDelegate.m";
    }
    else if ([dropboxAppSecret rangeOfCharacterFromSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]].location != NSNotFound)
    {
        errMsg = @"Make sure you set the app secret correctly in DBRouletteAppDelegate.m";
    }
    else if (dropboxAppType.length == 0)
    {
        errMsg = @"Set your root to use either App Folder of full Dropbox";
    }
    else
    {
        NSString * plistPath = [[NSBundle mainBundle] pathForResource:@"Info" ofType:@"plist"];
        NSData * plistData = [NSData dataWithContentsOfFile:plistPath];
        NSDictionary * loadedPlist = [NSPropertyListSerialization propertyListWithData:plistData options:0 format:NULL error:NULL];
        NSString * scheme = [[[[loadedPlist objectForKey:@"CFBundleURLTypes"] objectAtIndex:0] objectForKey:@"CFBundleURLSchemes"] objectAtIndex:0];
        if ([scheme isEqual:@"db-w1soeoslqj8x50h"]) {
            errMsg = @"Set your URL scheme correctly in Info.plist";
        }
    }
    
    DBSession * session = [[DBSession alloc] initWithAppKey:dropboxAppKey appSecret:dropboxAppSecret root:dropboxAppType];
    session.delegate = self;
    [DBSession setSharedSession:session];
    [DBRequest setNetworkRequestDelegate:self];
    
    if ([[DBSession sharedSession] isLinked]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowPhotoViewNotification" object:nil];
    }
    
    if (errMsg != nil) {
        [[[UIAlertView alloc] initWithTitle:@"Error Configuring Session" message:errMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    
    NSURL * launchURL = [launchOptions objectForKey:UIApplicationLaunchOptionsURLKey];
    if (launchURL) {
        [self application:application openURL:launchURL sourceApplication:nil annotation:nil];
        return NO;
    }
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([[DBSession sharedSession] handleOpenURL:url]) {
        if ([[DBSession sharedSession] isLinked]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowPhotoViewNotification" object:nil];
        }
        return YES;
    }
    return NO;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - DBSessionDelegate Methods
- (void)sessionDidReceiveAuthorizationFailure:(DBSession *)session userId:(NSString *)userId
{
    _reLinkUserId = [userId copy];
    [[[UIAlertView alloc] initWithTitle:@"Dropbox Session Ended"  message:@"Do you want relink?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Relink", nil] show];
}


#pragma mark - UIAlertViewDelegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != alertView.cancelButtonIndex) {
        [[DBSession sharedSession] linkUserId:_reLinkUserId fromController:_window.rootViewController];
        _reLinkUserId = nil;
    }
}

#pragma mark - DBNetworkRequestDelegate Methods

static int outstandingRequests;

- (void)networkRequestStarted
{
    outstandingRequests++;
    if (outstandingRequests == 1) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    }
}

- (void)networkRequestStopped
{
    outstandingRequests--;
    if (outstandingRequests == 0) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
}

@end
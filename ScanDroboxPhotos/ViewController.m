//
//  ViewController.m
//  ScanDroboxPhotos
//
//  Created by Alexander on 12/09/15.
//  Copyright (c) 2015 Alexander. All rights reserved.
//

#import "ViewController.h"
#import <DropboxSDK/DropboxSDK.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showPhotoViewController) name:@"ShowPhotoViewNotification" object:nil];
    
    if ([[DBSession sharedSession] isLinked]) {
        NSLog(@"linked");
        [self performSelector:@selector(showPhotoViewController) withObject:nil afterDelay:0.3f];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateButtons];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (IBAction)btnLinkPressed:(id)sender {
    NSLog(@"Link Button Pressed");
    
    if (![[DBSession sharedSession] isLinked]) {
        [[DBSession sharedSession] linkFromController:self];
    }
    else
    {
        [[DBSession sharedSession] unlinkAll];
        [[[UIAlertView alloc] initWithTitle:@"Account Uninked!" message:@"Your dropbox account unlinked!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        [self updateButtons];
    }
}

#pragma mark - Methods
- (void)updateButtons {
    NSString* title = [[DBSession sharedSession] isLinked] ? @"Unlink Dropbox" : @"Link Dropbox";
    [_btnLink setTitle:title forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem.enabled = [[DBSession sharedSession] isLinked];
}

- (void)showPhotoViewController
{
    NSLog(@"showPhotoViewController");
    [self performSegueWithIdentifier:@"showPhotoViewController" sender:nil];
}

@end

//
//  ViewController.h
//  ScanDroboxPhotos
//
//  Created by Alexander on 12/09/15.
//  Copyright (c) 2015 Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnLink;

- (IBAction)btnLinkPressed:(id)sender;
@end


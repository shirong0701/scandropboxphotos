//
//  GalleryViewController.h
//  ScanDroboxPhotos
//
//  Created by Alexander on 13/09/15.
//  Copyright (c) 2015 Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <DropboxSDK/DropboxSDK.h>

@interface GalleryViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, DBRestClientDelegate, UIAlertViewDelegate>
{
    NSMutableArray *assetGroups;
    NSOperationQueue *queue;
    ALAssetsLibrary * library;
    ALAssetsGroup * assetGroup;
    
    DBRestClient * restClient;
    BOOL isUploading;
}

@property (weak, nonatomic) IBOutlet UICollectionView *galleryView;

@property (strong, nonatomic) NSMutableArray * thumbArray;
@property (strong, nonatomic) NSMutableArray * imgeArray;
@property (assign, nonatomic) NSInteger selectedIndex;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

- (IBAction)btnCancelPressed:(id)sender;

@end
